# ForkBox
A Web Application Portal for your clients!

* Make sure you have [docker installed](https://docs.docker.com/install) on the machine you're hosting this app on.
* Install [Docker-Compose](https://docs.docker.com/compose/install) in addition on the machine you're using to develop this app.
* Run `docker-compose up --build`

## Database Setup
**On first setup, you will need to create a database and seed it with data. To do this, we use [Phinx](https://phinx.org)**

* Enter the container, run `docker-compose exec forkbox bash`
* run `cd /var/www/html && ./phinx migrate`

Check your browser at `http://localhost:5001/` to see ForkBox in action.

## Front-End Development Only
**This is for front-end code and styling using React.**

Just run `cd ./front-react && npm i && npm start`

# Deployment Testing

Preview the Master Branch's build @ [forkbox.io](http://forkbox.io).
