import {createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const initialState = {
  auth: {
    userdata: {},
    loggedIn: false,
    loading: true
  },
  apps: {
    list: [],
    groups: [],
    sidebarToggle: localStorage.getItem('sidebar-toggle'),
    loading: true
  }
};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware)
);

export default store;
