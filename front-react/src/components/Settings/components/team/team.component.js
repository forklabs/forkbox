import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import FontAwesome from 'react-fontawesome';
// import { connect } from 'react-redux';

class Team extends Component {
  render() {
    let teams = {
      "admin": {
        user_id: 1,
        user_firstname: "Administrator",
        user_lastname: "",
        username: "admin"
      }
    };

    return (
      <div id="fbx-settings-page-team" className="fbx-settings-page">
      <p id="apps_list-page_description">
        Here's a list of your <b>Team Members</b>.
      </p>
      <div id="new_db-button">
        <a href="/team/invite">Invite Members +</a>
      </div>
      <div id="apps_list-list_container">
        <ul id="apps_list-list">
          <li id="team_list-member-{team_member.user_id}" class="team_list-member">
            <div class="team_list-avatar"><div class="team_list-avatar-img"><img alt="{team_member.username}" src="http://img.forkboxes.com/{team_member.user_id}.jpg" /></div></div>
            <div class="team_list-name">{teams.admin.user_firstname} {teams.admin.user_lastname}  ({teams.admin.username})</div>
            <div class="team_list-role">
              <select>
                <option>Owner</option>
                <option>Project Manager</option>
                <option>Writer</option>
                <option>Designer</option>
                <option>Developer</option>
                <option>CTO</option>
                <option>CFO</option>
                <option>+ New Role</option>
              </select>
            </div>
            <div class="team_list-edit"><button>Edit</button></div>
          </li>
        </ul>
      </div>
      </div>
    )
  }
}

export default Team;
