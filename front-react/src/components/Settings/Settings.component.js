import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';

import General from './components/general/general.component';
import Apps from './components/apps/apps.component';
import Connections from './components/connections/connections.component';
import Security from './components/security/security.component';
import Team from './components/team/team.component';

import './Settings.css';
/*
Here's a list of your <b>Team Members</b>.

No team members yet, something is wrong.. You shouldn't be seeing this. <br />

*/


const presetNavLinks = [
    {
  			id: '31',
  			label: 'General',
  			path: '/settings',
        icon: 'cogs',
        view: General
    },
    {
  			id: '32',
  			label: 'Connections',
  			path: '/settings/connections',
        icon: 'plug',
        view: Connections
    },
    {
  			id: '33',
  			label: 'Apps',
  			path: '/settings/apps',
        icon: 'rocket',
        view: Apps
    },
    {
  			id: '34',
  			label: 'Team',
  			path: '/settings/team',
        icon: 'sitemap',
        view: Team
    },
    {
  			id: '35',
  			label: 'Security',
  			path: '/settings/security',
        icon: 'fingerprint',
        view: Security
    }
  ];

class NavLinks extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }
  render() {
    return this.props.navLinks.map(navLink => {
      let isActive = '';
      if(this.props.current === navLink.path)
        isActive = ' fbx-link-active';

      return(
        <li key={navLink.id} id={`fbx-settings-nav-link-${navLink.label}`} className={`fbx-settings-nav-link-li${isActive}`}>
          <Link title={navLink.label} to={navLink.path} className={`fbx-settings-nav-link`}><span className="fbx-settings-nav-link-label">{navLink.label}</span> <FontAwesome name={navLink.icon} /></Link>
        </li>
      );
  })
  }
}

class SettingsSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = props;
    console.log('settings sidebar props', this.props);
  }
  render() {
    return(
      <nav id="settings-nav">
        <h1 class="nav-header"><FontAwesome name='cog' /> <span class="nav-header-text">Settings</span></h1>
        <ul>
          <NavLinks current={this.props.location.pathname} navLinks={presetNavLinks} />
        </ul>
      </nav>
    );
  }
}

class SettingView extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }
    render() {
      console.log('settingView state', this.props);
      let navLink = this.props.link;
      let ViewComponent = navLink.view;
      return(
        <div id="settings-view">
          <h1 id="apps_list-header">{navLink.label.toUpperCase()}</h1>
          <ViewComponent {...this.props} />
        </div>

      );
    }
}

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = props;
  }

  componentWillMount() {
    console.log('settings will mount', this.props, this.state);
    this.props = this.state;
  }
  render() {
    console.log('rendering settings', this.props, this.state);

    // Decide which view to load based on location pathname
    presetNavLinks.map(navLink => {
      if(navLink.path === this.props.location.pathname) {
        console.log('updating viewComponent', navLink.view);
        this.props = {
          ...this.props,
          link: navLink
        };
      }
      return null;
    });

    return (
      <div id="settings-wrap">
        <SettingsSidebar {...this.props}/>
        <SettingView {...this.props}/>

      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(Settings);
