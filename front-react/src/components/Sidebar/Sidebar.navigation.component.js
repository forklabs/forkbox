import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';

const presetNavLinks = [
    {
  			id: '1',
  			label: 'Dashboard',
  			path: '/',
        icon: 'chart-line'
    },
    {
  			id: '2',
  			label: 'Workspace',
  			path: '/workspace',
        icon: 'user-astronaut',
    },
    {
  			id: '3',
  			label: 'Settings',
  			path: '/settings',
        icon: 'cog'
    }
  ];

class NavLinks extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }
  render() {
    return this.props.navLinks.map(navLink => {
      console.log('nav props', this.props);
      let isActive = '';
      if(this.props.current === navLink.path) isActive = ' fbx-sidebar-nav-link-active';
      return(
        <li key={navLink.id} id={`fbx-sidebar-nav-link-app-${navLink.label}`} className={`fbx-sidebar-nav-link-container${isActive}`}>
          <Link title={navLink.label} to={navLink.path} className={`fbx-sidebar-nav-link${isActive}`}><span className="fbx-sidebar-nav-link-label">{navLink.label}</span> <FontAwesome name={navLink.icon} /></Link>
          <div className="fbx-sidebar-nav-link-pointer"></div>
        </li>
      );
  })
  }
}

class SidebarNav extends Component {
  constructor(props) {
    super(props);
    console.log('sidebarnav props', this.props);
  }
  render() {
    return (
      <div id="fbx-sidebar-nav-wrap">
        <nav id="fbx-sidebar-nav">
          <ul id="fbx-sidebar-nav-list">
            <NavLinks current={this.props.location.pathname} navLinks={presetNavLinks}/>
          </ul>
        </nav>
      </div>
    )}
  };

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(SidebarNav);
