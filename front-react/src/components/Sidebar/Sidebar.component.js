import React, { Component } from 'react';
import './Sidebar.css';
import SidebarNav from './Sidebar.navigation.component';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { toggleSidebar } from '../../actions/appActions';


class SideBar extends Component {
  constructor(props) {
    super(props);

    this.state = this.props;
  }

  componentWillMount() {
    console.log('sidebar initial state:', this.state, this.props);
  }

  async componentWillReceiveProps(newProps) {
    console.log('Sidebar Received Props:', newProps);
    this.setState(newProps);
    console.log('new sidebar state', this.state);
  }

  async toggleSidebar(e) {
    this.props.toggleSidebar();
    this.setState(this.props);
    console.log('Sidebar Toggle Status:', localStorage.getItem('sidebar-toggle'));
  }

  render() {
    if(this.props.auth.loggedIn) {
      return (
        <div id="fbx-content-sidebar" className={`sidebar-${this.props.apps.sidebarToggle}`}>
          <div id="fbx-sidebar-btn">
            <button id="fbx-sidebar-button" onClick={e => this.toggleSidebar(e)}>
              <FontAwesome name="bars" />
            </button>
          </div>
    		  <div id="fbx-sidebar-content">
  		 		  <div id="fbx-sidebar-header-wrap">
   			 	    <a href="/" id="fbx-sidebar-header-logo" className="fbx-link">
  		 			    <img id="fbx-sidebar-header-logo-img" alt="Team Logo" src="/img/fbx/forkbox-nav-logo.png" />
   			 			</a>
  		 			</div>
  		 			<div id="fbx-sidebar-nav-wrap">
              <SidebarNav location={this.props.location} />
              <div id="fbx-sidebar-info">
                <span id="fbx-sidebar-copy">
                Powered By
                </span>&nbsp;
                <a href="https://forkbox.io" className="text-forklabsgreen">ForkBox</a>
                <br />
                <a href="http://forkboxes.com/legal/terms" className="text-forklabsgreen">Terms</a> | <a href="/developers" className="text-bloodred">Developers</a>
              </div>
            </div>
            <div id="fbx-sidebar-userdata">
              <div id="fbx-sidebar-username">
                <Link to={`/logout`} activeClassName="active">
                  {this.props.auth.loggedIn ? this.props.auth.userdata.username : 'Login' } &nbsp;<FontAwesome name='chevron-down' />
                </Link>
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      console.log('user not logged in', this.props);
      return(
        <>
        </>
      )
    }
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {toggleSidebar})(SideBar);
