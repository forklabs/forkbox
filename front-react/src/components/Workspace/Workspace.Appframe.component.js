import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getApp } from '../../actions/appActions';
/*import { Link } from 'react-router-dom';*/
import './Workspace.css';
import './WorkspaceElements.css';


class Appframe extends Component {
  async componentWillMount() {
    await this.props.getApp(this.props.match.params.app_slug);
  }

  getFrameSrc(e) {
    console.log('frame src', e);
    console.log('frames', window.frames);
  }

  render() {
    console.log('the props', this.props);

    if(this.props.apps.app !== undefined) {
      return(
        <div id="fbx-panel-workspace" className="fbx-major-panel">
          <iframe
            src={this.props.apps.app.app_uri + '?auth_key=' + this.props.apps.app.connection.connection_auth.auth_key}
            title={this.props.apps.app.app_name}
            width="100%"
            height="100%"
            display="initial"
            onLoad={this.getFrameSrc}
          />
        </div>
      );
    } else {
      return(
        <div id="fbx-panel-workspace-error">
          <p>
            No App Loaded
          </p>
        </div>
      );
    }
  }

}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {getApp})(Appframe);
