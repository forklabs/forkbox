import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getApps } from '../../actions/appActions';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import './Workspace.css';
import './WorkspaceElements.css';

class AppItem extends Component {
    render() {
      console.log('appItem props', this.props);
      let app_href = `/workspace/app/${this.props.app_slug}`;
      return(
        <div id="apps_list-app-{this.props.app_id}" class="apps_list-app col-sm-4">
          <Link
            title={this.props.app_name}
            to={app_href}>
            <div class="apps_list-app-tile">
              {this.props.app_name}
            </div>
            <div class="apps_list-app-description">
              by <span class="text-forklabsgreen">{this.props.app_author}</span>
            </div>
          </Link>
        </div>
      );
    }
}

class AppList extends Component {
  render() {
    if(this.props.apps.loading === true) {
      return(
        <div id="loading_container">
          <div id="loading_spinner" className="fa-2x fa-spin">
            <FontAwesome name='flask' />
            <FontAwesome name='vial' />
          </div>
        </div>
      );
    } else {
      if(this.props.apps.list.length) {
        return this.props.apps.list.map(app => {
          return(
            <div id="apps_list-list_container" className="container">
              <AppItem {...app}/>
            </div>
          );
        });
      } else {
        return(
          <p id="apps_list-page_description">
            You have no Apps installed yet.
          </p>
        );
      }
    }
  }
}


class Workspace extends Component {
  async componentWillMount() {
    await this.props.getApps();
  }
  render() {
    return (
      <div id="fbx-panel-workspace" className="fbx-major-panel">
        <div id="apps_list-wrap">
          <div id="fbx_page-head">
            <h1 id="apps_list-header">Workspaces</h1>
          </div>
          <AppList {...this.props} />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {getApps})(Workspace);
