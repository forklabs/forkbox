import React, {Component} from 'react';
import { connect } from 'react-redux';
import { checkJwt } from '../../actions/authActions';
import Route from 'react-router-dom/Route';
import { withRouter } from 'react-router';
import Dashboard from '../../components/Dashboard/Dashboard.component';
import Workspace from '../../components/Workspace/Workspace.component';
import Appframe from '../../components/Workspace/Workspace.Appframe.component';
import Settings from '../../components/Settings/Settings.component';
import Login from './Login.component';

let routes = [
  {
    path: '/',
    component: Dashboard,
    exact: true,
    key: 'dashboard'
  },
  {
    path: '/workspace',
    component: Workspace,
    exact: true,
    key: 'workspace'
  },
  {
    path: '/workspace/app/:app_slug',
    component: Appframe,
    exact: true,
    key: 'appframe'
  },
  {
    path: '/settings',
    component: Settings,
    exact: false,
    key: 'settings'
  }
];

class AuthenticatedComponent extends Component {
  render() {
    console.log('this.props.auth', this.props.auth)
    if(!this.props.auth.loading && this.props.auth.loggedIn) {
      return (
        <div id="fbx-content-canvas" className={`sidebar-${this.props.apps.sidebarToggle}`}>
          {routes.map(route => {
            let path = route.path,
                key = route.key,
                exact = route.exact,
                View = route.component;

            if(exact === true) {
              return(
                <Route
                  path={path}
                  exact
                  key={key}
                  render={(props) => <View {...props} />}
                />
              )
            } else {
              return(
                <Route
                  path={path}
                  key={key}
                  render={(props) => <View {...props} />}
                />
              )
            }
          })}
        </div>
      )
    } else {
      if(!this.props.auth.loading) {
        return (
            <div id="fbx-content-canvas" className="canvas-login">
              <Login />
            </div>
        )
      } else {
        return (
          <div id="fbx-content-canvas" className="canvas-loading">
            <h2 id="fbx-loading">Loading</h2>
          </div>
        );
      }
    }
  }
}

function mapStateToProps(state) {
  return state;
}

export default withRouter(connect(mapStateToProps, {checkJwt})(AuthenticatedComponent));
