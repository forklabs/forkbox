import React, {Component } from 'react';
import { connect } from 'react-redux';
import { userLogin } from '../../actions/authActions';
import './Login.css';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    }
  }
  change(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  async submit(e) {
    e.preventDefault();
    let formBody = new FormData();
    formBody.set('email', this.state.email);
    formBody.set('password', this.state.password);
    let loginResponse = await this.props.userLogin(formBody);
    console.log('loginResponse', loginResponse);
  }

  render() {
    if(this.props.auth.loggedIn) {
      this.props.history.push('/');
    } else {

      return(
      		<>
      			<div id="team-logo">
      				<img src="/js/public/forkboxlogo.png" alt="ForkBox" title="ForkBox" />
      			</div>
      			<div id="forkbox-login-box">
      				<form onSubmit={e => this.submit(e)} id="forkbox-login-form" method="post" action="/auth/login">
      					<input
                  type="text"
                  name="email"
                  className="forkbox-login-field"
                  id="forkbox-login-email"
                  title="Your Email"
                  placeholder="Email Address"
                  onChange={e => this.change(e)}
                  value={this.state.email}
                  autocomplete="off"
                />
      					<br />
      					<input
                  type="password"
                  name="password"
                  className="forkbox-login-field"
                  id="forkbox-login-password"
                  title="Your Password"
                  placeholder="Password"
                  onChange={e => this.change(e)}
                  value={this.state.password}
                  autocomplete="off"

                />
      		      <br />
      					<input
                  type="submit"
                  value="Login"
                  id="forkbox-login-submit_button"
                />
      				</form>
      			</div>
      		</>
      )
    }
  }

}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, {userLogin})(Login);
