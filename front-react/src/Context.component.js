import axios from 'axios';
import getJwt from './components/Authentication/jwt.helper.js';
import React, { Component } from 'react';
const MyContext = React.createContext();

export class Provider extends Component {
  state = {
    userdata: {},
    loggedIn: false
  }

  checkJwt = async () => {
    // Retrieve JSON web token from client
    console.log('app will mount', this.state);
    let jwt = getJwt();
    if(!jwt) {
      console.log('jwt not valid', jwt);
      //this.props.history.push('/');
    } else {
      // check if the JWT is authorized
      await axios
        .get('/user', {
          headers: {
            Authorization: `Bearer ${jwt}`
          }
        })
        .then(res => {
          let data = res.data;
          let newState = {
            _id: data.user_id,
            username: data.username,
            team: {
              _id: data.team_id,
              name: data.team_name,
              friendly_name: data.team_name.toLowerCase(),
              logo: data.team_logo
            }
          };
          this.setState({
            userdata: newState,
            loggedIn: true
          })
          return true;
        })
        .catch(err => {
        console.log('err', err);
        // localStorage.removeItem('fbx-jwt')
        //this.props.history.push('/login')
          this.setState({
            userdata: undefined,
            loggedIn: false
          })
      })
    }
  }
  render() {
    return(
      <MyContext.Provider value={{
          ...this.state,
          checkJwt: this.checkJwt
        }}
      >
        {this.props.children}
      </MyContext.Provider>
    )
  }
}
export const Consumer = MyContext.Consumer;
