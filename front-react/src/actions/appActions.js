import { TOGGLE_SIDEBAR, GET_USER_APPS, GET_USER_APPS_FAIL, GET_USER_CONNECTION, GET_USER_CONNECTION_FAIL } from './types';
import axios from 'axios';

export const toggleSidebar = () => (dispatch) => {
  let toggle = localStorage.getItem('sidebar-toggle');
  console.log('toggle action', toggle);
  if(toggle === 'close' && toggle !== 'open')
    toggle = 'open';
  else
    toggle = 'close';

  localStorage.setItem('sidebar-toggle', toggle);

  dispatch({
    type: TOGGLE_SIDEBAR,
    toggle: toggle
  })
};

export const getApps = () => (dispatch) => {
  let jwt = localStorage.getItem('fbx-jwt');
  if(!jwt) {
    return false;
  } else {
    // check if the JWT is authorized'
    axios
      .get('/workspace/apps/json', {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(res => {
        dispatch({
          type: GET_USER_APPS,
          list: res.data
        })
      })
      .catch(res => dispatch({
        type: GET_USER_APPS_FAIL,
        error: res.error
      }));
  }
};

export const getApp = (app_slug) => (dispatch) => {
  let jwt = localStorage.getItem('fbx-jwt');
  if(!jwt) {
    return false;
  } else {
    // check if the JWT is authorized'
    axios
      .get(`/workspace/app/${app_slug}/json`, {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(res => {
        console.log('workspace/app', res.data);

        if(res.data.user_subscribed) {
          dispatch({
            type: GET_USER_CONNECTION,
            app: res.data.app
          })
        } else {
          dispatch({
            type: GET_USER_CONNECTION_FAIL,
            error: res.data.error
          })
        }
      })
      .catch(res => dispatch({
        type: GET_USER_CONNECTION_FAIL,
        error: res.error
      }));
  }
};
