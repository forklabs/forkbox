import { CHECK_JWT, CHECK_JWT_FAIL, USER_LOGIN } from './types';
import axios from 'axios';

export const checkJwt = () => (dispatch) => {
  let jwt = localStorage.getItem('fbx-jwt');
  if(!jwt) {
    dispatch({
      type: CHECK_JWT_FAIL,
      error: 'Token not defined'
    })

    return false;
  } else {
    // check if the JWT is authorized'
    axios
      .get('/user', {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(res => {
        console.log('fetched userdata', res.data);
        if(res.data.logged_in) {
          console.log('its using this one');
          dispatch({
            type: CHECK_JWT,
            userdata: res.data
          })
        } else {
          console.log('fail data', res.data)
          dispatch({
            type: CHECK_JWT_FAIL,
            error: res.data.reason
          })
        }
      })
      .catch(res => dispatch({
        type: CHECK_JWT_FAIL,
        error: res.error
      }));
  }
};

export const userLogin = (formBody) => dispatch => {
  return axios({
    method: 'post',
    url: '/auth/login',
    data: formBody,
    config: { headers: { 'Content-Type': 'application/x-www-form-urlencoded'}}
  })
  .then(res => {
    if(res.data.status === 'SUCCESS') {
      console.log('res data', res.data);
      console.log('logging token', localStorage.setItem('fbx-jwt', res.data.token));
      dispatch({
        type: USER_LOGIN,
        userdata: res.data.userdata,
        loggedIn: true
      })
    } else {
      console.log('err, res.data', res.data);
    }
  })
  .catch(err => {
    console.log('Error', err)
  })
};
