import React, { Component } from 'react';
import './App.css';

import Sidebar from './components/Sidebar/Sidebar.component';
import Auth from './components/Authentication/Authenticated.component';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import { connect } from 'react-redux';
import { checkJwt } from './actions/authActions';


class App extends Component {
  async componentWillMount() {
    let userdata = await this.props.checkJwt();
    console.log('userdata', userdata);
  }
  render() {
    //console.log('userdata is', userdata);
    return (
        <Router>
          <Switch>
            <>
              <Route
                path="/"
                render={(props) => <Sidebar {...props} />}
              />
              <Route
                path="*"
                render={(props) => <Auth {...props} />}
              />
            </>
          </Switch>
        </Router>
    );
  }
}

function mapStateToProps(state) {
  return state;
}
/*
function mapDispatchToProps(dispatch) {
  return {
    deletePost: () => {
       dispatch({type: 'DELETE_POST', id: id})
    }
  };
}
*/
export default connect(mapStateToProps, {checkJwt})(App);
