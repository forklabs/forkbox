import { TOGGLE_SIDEBAR, GET_USER_APPS, GET_USER_APPS_FAIL, GET_USER_CONNECTION, GET_USER_CONNECTION_FAIL } from '../actions/types';

const initialState = {
  list: [],
  app: {
    host: null,
    key: null,
    name: null
  },
  sidebarToggle: 'open',
  loading: true
};

const dispatch = (state = initialState, action) => {
  let stateObj;
  switch(action.type) {
    case GET_USER_APPS:
      stateObj = {
        ...state,
        list: action.list,
        loading: false
      };
    break;
    case GET_USER_APPS_FAIL:
      stateObj = {
        ...state,
        loading: false,
        error: action.error
      };
    break;
    case GET_USER_CONNECTION:
      stateObj = {
        ...state,
        app: action.app
      };
    break;
    case GET_USER_CONNECTION_FAIL:
      stateObj = {
        ...state,
        error: action.error
      };
    break;
    case TOGGLE_SIDEBAR:
      stateObj = {
        ...state,
        sidebarToggle:  action.toggle
      }
    break;
    default:
      stateObj = state;
  }

  return stateObj;
};

export default dispatch;
