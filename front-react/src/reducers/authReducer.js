import { CHECK_JWT, USER_LOGIN, CHECK_JWT_FAIL, USER_LOGOUT } from '../actions/types';

const initialState = {
  userdata: {},
  loggedIn: false,
  loading: true
};

const dispatch = (state = initialState, action) => {
  let stateObj;
  switch(action.type) {
    case CHECK_JWT:
      stateObj = {
        ...state,
        userdata: action.userdata,
        loggedIn: true,
        loading: false
      };
    break;
    case USER_LOGIN:
      stateObj = {
        ...state,
        userdata: action.userdata,
        loggedIn: true,
        loading: false
      };
    break;
    case CHECK_JWT_FAIL:
      stateObj = {
        ...state,
        loggedIn: false,
        loading: false,
        error: { message: "JWT Check Fail." }
      };
    break;
    case USER_LOGOUT:
      stateObj = {
        ...state,
        loggedIn: false,
        loading: false
      };
    break;
    default:
      stateObj = state;
  }

  return stateObj;
};

export default dispatch;
