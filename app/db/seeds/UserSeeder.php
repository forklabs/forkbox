<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
      $data = [
        [
          'username' => 'admin',
          'user_password' => md5('root'),
          'user_firstname' => 'root',
          'user_lastname' => 'administrator',
          'user_appdata' => '2',
          'user_email' => 'admin@localhost.com',
          'team_id' => 1,
          'team_role' => 'owner'
        ]
      ];

      $users = $this->table('forkbox_users');
      $users->insert($data)
        ->saveData();
    }
}
