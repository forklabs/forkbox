<?php


use Phinx\Migration\AbstractMigration;

class InitialSetup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      define('PREFIX', 'forkbox_');

      # Apps
      $apps = $this->table(PREFIX . 'apps', ['id' => 'app_id']);
      $apps->addColumn('app_name', 'string', [ 'limit' => 60, 'null' => false])
              ->addColumn('app_description', 'text', [ 'limit' => 0, 'null' => false])
              ->addColumn('app_author', 'integer', [ 'limit' => 10, 'null' => false])
              ->addColumn('app_slug', 'string', [ 'limit' => 60, 'null' => false])
              ->addColumn('app_creation_time', 'datetime', [ 'null' => true])
              ->addColumn('app_uri', 'string', [ 'limit' => 255, 'null' => true])
              ->addColumn('app_client', 'string', [ 'limit' => 255, 'null' => true])
              ->addColumn('app_secret', 'string', [ 'limit' => 255, 'null' => true])
              ->addColumn('app_published', 'integer', [ 'limit' => 1, 'null' => false, 'default' => 0])
              ->addIndex(['app_id', 'app_slug', 'app_client', 'app_published'])
              ->create();

      # Connections
      $connections = $this->table(PREFIX . 'connections', ['id' => 'connection_id']);
      $connections->addColumn('connection_name', 'string', ['limit' => 60, 'null' => false])
              ->addColumn('connection_type', 'enum', ['values' => ['app', 'db', 'email', 'slack', 'google', 'dropbox', 'facebook', 'instagram', 'discord']])
              ->addColumn('connection_details', 'text', ['limit' => 0, 'null' => true])
              ->addColumn('connection_auth', 'text', ['limit' => 0, 'null' => true])
              ->addColumn('connection_date', 'datetime', ['null' => true])
              ->addIndex(['connection_id'])
              ->create();

      # Users
      $users = $this->table(PREFIX . 'users', ['id' => 'user_id']);
      $users->addColumn('username', 'string', ['limit' => 60])
              ->addColumn('user_password', 'string', ['limit' => 255])
              ->addColumn('user_firstname', 'string', ['limit' => 60])
              ->addColumn('user_lastname', 'string', ['limit' => 60])
              ->addColumn('user_regdate', 'datetime', ['null' => true])
              ->addColumn('user_privs', 'text', ['limit' => 0, 'default' => ''])
              ->addColumn('user_appdata', 'text', ['limit' => 0])
              ->addColumn('user_email', 'string', ['limit' => 255])
              ->addColumn('team_id', 'integer', ['limit' => 32])
              ->addColumn('team_role', 'string', ['limit' => 5])
              ->addIndex(['user_id', 'username', 'user_email', 'team_id', 'team_role'])
              ->create();

      # Sessions
      $sessions = $this->table(PREFIX .'sessions', ['id' => 'session_id']);
      $sessions->addColumn('session_token', 'string', ['limit' => 255])
              ->addColumn('session_user', 'integer', ['limit' => 32])
              ->addColumn('session_ip', 'string', ['limit' => 20])
              ->addColumn('session_useragent', 'text', ['limit' => 0])
              ->addColumn('session_time_initiated', 'timestamp', ['null' => true, 'default' => 'CURRENT_TIMESTAMP'])
              ->addIndex(['session_id', 'session_token', 'session_ip'])
              ->create();

      # Teams
      $teams = $this->table(PREFIX . 'teams', ['id' => 'team_id']);
      $teams->addColumn('team_name', 'string', ['limit' => 40, 'null' => true])
            ->addColumn('team_admin', 'integer', ['limit' => 11, 'null' => true])
            ->addColumn('team_domain', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('team_logo', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('team_groups', 'text', ['limit' => 0, 'null' => true])
            ->addIndex(['team_id', 'team_name', 'team_admin', 'team_domain'])
            ->create();

      # api Keys
      $api_keys = $this->table(PREFIX . 'api_keys',  ['id'=> 'key_id']);
      $api_keys->addColumn('key_last_active', 'string', ['limit' => 8])
              ->addColumn('key_value', 'string', ['limit' => 255])
              ->addColumn('key_user_id', 'integer', ['limit' => 10])
              ->addColumn('key_app_id', 'integer', ['limit' => 10])
              ->addColumn('key_connection_id', 'integer', ['limit' => 10, 'null' => false])
              ->addColumn('key_permissions', 'text', ['limit' => 0, 'null' => true])
              ->addColumn('created', 'datetime', ['null' => true])
              ->addColumn('key_last_activity', 'datetime', ['null' => true])
              ->addIndex(['key_user_id', 'key_app_id'])
              ->create();
    }
}
