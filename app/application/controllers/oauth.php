<?php require(PROTECT);

	class Oauth extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();

			/* oAuth Server */
				// Autoloading (composer is preferred, but for this example let's just do this)
				require_once(VENDOR_PATH.'bshaffer/oauth2-server-php/src/OAuth2/Autoloader.php');
				OAuth2\Autoloader::register();

				// $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
				$storage = new OAuth2\Storage\Pdo(array(
					'dsn' => 'mysql:dbname='.DB_DATABASE.';host='.DB_HOST,
					'username' => DB_USER,
					'password' => DB_PASSWORD
				));

				// Pass a storage object or array of storage objects to the OAuth2 server class
				$this->server = new OAuth2\Server($storage);

				// Add the "Client Credentials" grant type (it is the simplest of the grant types)
				$this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

				// Add the "Authorization Code" grant type (this is where the oauth magic happens)
				$this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
			/* -- End oAuth Server */

			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');
			$this->connect = $this->model('connect_model', array('team' => $this->team, 'apps' => $this->apps));
			$this->page_vars = new stdClass();
			
			$this->token_data = $this->auth->is_authenticated();

			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			if($this->team_data = $this->team->get_team_data($team_domain)[0])
				$team_id = $this->team_data['team_id'];
			else {
				header("Location: http://forkbox".TLD."/");
			}

			if($this->token_data['is_authenticated'] == 0)
				YU_redirect('auth');
			else
				$this->user_id = $this->token_data['token_data']['session_user'];

				$this->header_vars = new stdClass();
				$userdata = $this->users->get_user_data($this->user_id);
				$this->header_vars->userdata = $userdata;
		}

		function index() {
			// We might do something here i guess lol
			$this->view->load('fbx_header', $this->header_vars);
			echo "oAuth is nice isn't it? More like a pain in the ass if you ask the guy who implemented it onto ForkBox. –Devontrae AKA Mr. Fork";
			$this->view->load('fbx_footer');
		}

		function authorize() {
			$defaultScope = 'connections';
			$supportedScopes = array(
			  'connections',
			  'database'
			);

			$memory = new OAuth2\Storage\Memory(array(
			  'default_scope' => $defaultScope,
			  'supported_scopes' => $supportedScopes
			));

			$scopeUtil = new OAuth2\Scope($memory);

			$this->server->setScopeUtil($scopeUtil);
			$this->page_vars->scope = $_GET['scope'];

			# Lets start off by showing a list of installed apps
			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('oauth/install_form', $this->page_vars);
			$this->view->load('fbx_footer');
		}

		function grant() {
			/*
			$request = OAuth2\Request::createFromGlobals();
			$response = new OAuth2\Response();

			// validate the authorize request
			if (!$server->validateAuthorizeRequest($request, $response)) {
					$response->send();
					die;
			}
			// print the authorization code if the user has authorized your client
			$is_authorized = ($_POST['authorized'] === 'Grant Access');
			$server->handleAuthorizeRequest($request, $response, $is_authorized);
			if ($is_authorized) {
			  // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
			  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
			  exit("SUCCESS! Authorization Code: $code");
			} else {
					// Let's do something else like redirect them back to the app's connect page
			}
			$response->send();
			*/
		}

		function token() {
			// Handle a request for an OAuth2.0 Access Token and send the response to the client
			$server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
		}

		function resource() {
			// Handle a request to a resource and authenticate the access token
			if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
			    $server->getResponse()->send();
			    die;
			}
			echo json_encode(array('success' => true, 'message' => 'You accessed my APIs!'));
		}

		function test() {

		}
	}
