<?php require(PROTECT);

	# Our default controller

	# Note: The default controller does not accept parameters such as http://forklabsllc.com/parameter1/parameter2
	# This means index($param1, $param2) would be invalid.

	class Workspace extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();
			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();
			$this->user_id = $this->token_data['userdata']['user_id'];
			$this->userdata = $this->users->get_user_data($this->user_id);
			$this->page_vars = new stdClass();
		}

		function index() {
			$this->page_vars->auth = $this->token_data;
			$this->page_vars->userdata = $this->userdata;
			$this->view->load('workspace', $this->page_vars);
		}

		function apps($api=null) {
		  if($api == 'json' || $_GET['api'] == 'json') {
		    header('Content-Type: application/json');
		    echo json_encode($this->users->get_user_connections($this->user_id, 'app'));
		  } else {
				redirect('workspace');
			}

		}

		function connection($connection_id=null) {
			if($connection_id) {
				$match = [];
				$match['connection_id'] = $connection_id;
				echo json_encode($this->users->get_user_connections($this->user_id, 'app', $match));
			} else {
				$this->page_vars->auth = $this->token_data;
				$this->page_vars->userdata = $this->userdata;
				$this->view->load('workspace', $this->page_vars);
			}
		}

		function app($app_slug=null,$type=null) {
			if($app_slug) {
				if($connection_id) $match['connection_id'] = $connection_id;

				$user_app = $this->users->get_user_connections($this->user_id, 'app', $app_slug);

				$this->page_vars->auth = $this->token_data;
				$this->page_vars->userdata = $this->userdata;

				if(strtolower($type) == 'json') {
					header('Content-Type: application/json');

					$response = new stdClass();
					$response->user_subscribed = false;

					if($user_app) {
						$response->app = $user_app;
						$response->user_subscribed = true;
					} else {
						$response->error = 'failed retrieving app connection';
					}

					echo json_encode($response);
				} else {
					$this->view->load('workspace', $this->page_vars);
				}
			} else {
				redirect('workspace');
			}
		}
		# This is necessary if you have CONFIG/ERROR_OVERRIDE set to true.
		# To set error override to true, visit application/config/config.php line: 35
		function error_override() {
			echo '404 NOT FOUND';
		}
	}
