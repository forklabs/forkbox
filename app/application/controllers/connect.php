<?php require(PROTECT);

	# Our Apps System
	# This is what MiniCMS will use to manage Apps.

	class Connect extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();

			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');
			$this->connect = $this->model('connect_model', array('team' => $this->team, 'apps' => $this->apps));

			$this->token_data = $this->auth->is_authenticated();

			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			if($this->team_data = $this->team->get_team_data($team_domain)[0])
				$team_id = $this->team_data['team_id'];
			else {
				header("Location: http://forkbox".TLD."/");
			}

			if($this->token_data['is_authenticated'] == 0)
				YU_redirect('auth');
			else
				$this->user_id = $this->token_data['token_data']['session_user'];

				$this->header_vars = new stdClass();
				$userdata = $this->users->get_user_data($this->user_id);
				$this->header_vars->userdata = $userdata;

		}

		function index() {
			# Lets start off by showing a list of installed apps
			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('connect/home', $this->users->get_user_connections($this->user_id, 'all'));
			$this->view->load('fbx_footer');
		}

		function search($set=null) {
			$show_view = true;
			switch($set) {
				case 'app':
				case 'apps':
					if($_POST['query']) {
						$json_return = $this->connect->get_connections("app", $_POST['query']);
						echo json_encode($json_return);
						$show_view = false;
					} else {
						$view = "connect/search/apps";
						$this->header_vars->title = "Find Apps";
					}
				break;
				default:
					$view = "connect/search/default";
			}

			if($show_view) {
				$this->view->load('fbx_header', $this->header_vars);
				$this->view->load($view);
				$this->view->load('fbx_footer');
			}
		}

		function edit($connection_id) {

		}
		function create($connect_data=null, $in_app=false) {
			if($connect_data) {
				# We need to generate the connect_data
				$parse_data = explode(',', $connect_data);
				$connect_data = array();
				if(is_array($parse_data) && count($parse_data) > 0) {
					foreach($parse_data as $data) {
						$pair = explode(':', $data);
						$connect_data[$pair[0]] = $pair[1];
					}
				}

				if(!$connect_data['type'])
					$connect_data['type'] = null;

				if($in_app == 'embed')
					$in_app = true;

				switch($connect_data['type']) {
					case 'db':
						if(!$_POST['connect_data']) {
							$output = new stdClass();
							$output->connection_type = 'db';
							$output->connect_data = $connect_data;
							$in_app = true;

							if($in_app)
								$this->view->load('fbx_header', $this->header_vars);
							$this->view->load('connect/create', $output);
							if($in_app)
								$this->view->load('fbx_footer');
						} else {
							parse_str($_POST['connect_data'], $connection_data);

							if(
								!$connection_data['db_host'] ||
								!$connection_data['db_user'] ||
								!$connection_data['db_password'] ||
								!$connection_data['db_name']
							) {
								$output = "Missing some Database Information";
								echo $output;
							} else {
								// We've got all the required data
								$connection_data['connection_type'] = 'db';

								if($access_level = $connection_data['access_level']) {
									switch(strtolower($access_level)) {
										case 'team':
											$access_param = $this->team_data['team_id'];
										break;
										case 'me':
										default:
											$access_level = null;
											$access_param = null;
									}
								} else {
									$access_level = null;
									$access_param = null;
								}

								// Create that connection
								$create = $this->connect->create_connection($connection_data, $this->user_id, $access_level, $access_param);

								if($create) {
									# For database connections, we need to submit to the Database Service to initialize
									$auth_details = json_decode($create['connection_auth']);
									print_r($auth_details);
									$auth_key = $auth_details->auth_key;
									$url = 'http://api.forkboxes.local/db/connect/init/key-'.$auth_key;

									$ch = curl_init();
									curl_setopt($ch,CURLOPT_URL, $url);
									$result = curl_exec($ch);
									curl_close($ch);

									$output = $result;

								}
							}
						}
					break;
					case 'app':
						# We'll do app connection stuff here
						# We're connecting a new app. Lets figure out what we need
						$app_id = $connect_data['app_id'];
						$app_data = $this->apps->get_app_data($app_id)[0];

						if($_POST['connect_data']) {
							# Lets create a connection
							$post_data = $_POST['connect_data'];
							$connection_details = array(
								'name' => $post_data['name'],
								'description' => $post_data['description'],
								'groups' => $post_data['groups'],
								'app_id' => $app_data['app_id'],
								'connection_type' => 'app'
							);

							$access_level = null;
							$access_param = null;

							$created_app = $this->connect->create_connection($connection_details, $this->user_id, $access_level, $access_param);
							echo json_encode($created_app);
						} else {
							# Lets show details about the app
							$output = new stdClass();
							$output->connection_type = 'app';
							$output->connect_data = $connect_data;
							$output->app_data = $app_data;
							$this->view->load('fbx_header', $this->header_vars);
							$this->view->load('connect/create', $output);
							$this->view->load('fbx_footer');
						}
					break;
					case null:
					default:
						//YU_redirect('connect');
						print_r($connect_data);
				}
			} else {
				# Display an interface for creating a connection
				echo "We need app details.";
			}
		}

		function id($connection_id, $function = null) {
			switch($function) {
				case 'edit':
					if($_POST['connect_data']) {
						# Submitting the connection edit
					} else {
						# Viewing the interface for editing a connection
					}
				break;
				case 'delete':
					if($_POST['connect_confirm']) {

					} else {
						# We display an interface for confirming deletion
					}
				break;
				case null:
				default:
					# We're just viewing the connection details
			}
		}
	}
