<?php require(PROTECT);

	# Our default controller

	# Note: The default controller does not accept parameters such as http://forklabsllc.com/parameter1/parameter2
	# This means index($param1, $param2) would be invalid.

	class Team extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();
			$this->users = $this->model('user_model');
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();

			if($this->token_data['is_authenticated'] == 0)
				YU_redirect('auth');
			else
				$this->user_id = $this->token_data['token_data']['session_user'];

			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			if($this->team_data = $this->team->get_team_data($team_domain)[0])
				$this->team_id = $this->team_data['team_id'];
			else
				header("Location: http://forkbox".TLD."/");

			$this->page_vars = new stdClass();

			$this->header_vars = new stdClass();
			$userdata = $this->users->get_user_data($this->user_id);
			$this->header_vars->userdata = $userdata;

		}

		function index() {
			$this->page_vars->team_members = $this->team->get_team_members($this->team_id);

			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('team/list', $this->page_vars);
			$this->view->load('fbx_footer');

		}

		function invite() {
			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('fbx_footer');
		}

		function groups($method=null) {
			switch($method) {
				case 'list':
					/*
					[ { "id": "Netta rufina", "label": "Red-crested Pochard", "value": "Red-crested Pochard" },
					*/
					$groups_list_arr = array();
					$team_groups = $this->team->fetch_groups($this->team_id);
					# print_r($team_groups);
					if($team_groups) {
						$out_arr = array();
						foreach($team_groups as $group) {
							$tmp_obj = new stdClass();
							$tmp_obj->id = $group->group_id;
							$tmp_obj->label = $group->group_name;
							$tmp_obj->value = $group->group_name;
							$out_arr[] = $tmp_obj;
						}
						$output = json_encode($out_arr);
					} else {
						$output = "[]";
					}
				break;
			}

			echo $output;
		}

		function members($method=null, $data=null) {
			switch($method) {
				case 'list':
					$out_arr = array();
					$team_members = $this->team->get_team_members($this->team_id);

					foreach($team_members as $member) {
						$tmp_obj = new stdClass();
						$tmp_obj->id = $member['user_id'];
						$tmp_obj->label = '@'.$member['username'];
						$tmp_obj->value = $member['user_id'];
						$out_arr[] = $tmp_obj;
					}
					$output = json_encode($out_arr);
				break;
			}

			echo $output;
		}

		function test($var1, $var2, $var3) {
			echo "Test Controller ".$var1.", ".$var2.", and ".$var3;
		}

		# This is necessary if you have CONFIG/ERROR_OVERRIDE set to true.
		# To set error override to true, visit application/config/config.php line: 35
		function error_override() {
			echo '404 NOT FOUND';
		}
	}
