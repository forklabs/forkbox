<?php require(PROTECT);

	class Auth extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();
			$this->team = $this->model('team_model');
			$this->page_vars = new stdClass();

			$this->page_vars->team_data = $this->team->get_team_data();

			# Pre-Auth Check
			$this->auth_data = $this->auth->is_authenticated();
			$this->token_data = $this->auth_data['token_data'];


			if($this->auth_data['is_authenticated'] == 0)
				$this->is_authenticated = 0;
			else
				$this->is_authenticated = 1;
		}

		function index() {
				$this->view->load('login_screen', $this->page_vars);
		}

		function login() {
			# Set the headers
			header('Content-type: application/json');
			$json_return = array();
			if(!empty($_POST)) {
				# This is a post method
				$email = $_POST['email'];
				$password = $_POST['password'];

				$token = md5(time()+rand());
				$team = $this->team_data[0]['team_id'];

				# Are we already logged in?
				if(!$this->is_authenticated) {
					# Lets go ahead and do the login thing
					$auth = $this->auth->authenticate(array(
						'email' => $email,
						'password' => $password,
						'token' => $token
					));

					#print_r($auth);
				#$authenticate_return;
					if($auth['status'] == 1) {
						# We successfully authenticated, the front end will redirect to the home page
							$json_return['status'] = 'SUCCESS';
							$json_return['token'] = $token;
							$json_return['userdata'] = $auth['userdata'];
					} else {
						# We did not successfully authenticate, lets return some json
						$json_return['status'] = 'FAILURE';
						$json_return['reason'] = $auth['reason'];
					}
				} else {
					# We are already authenticated, redirect them to the home page
					//YU_redirect('home');
					$json_return['status'] = 'FAILURE';
					$json_return['reason'] = 'already authenticated';
				}

				echo json_encode($json_return);
			} else {
				YU_redirect('auth');
			}
		}

		function logout() {
			if(!$this->is_authenticated) {
				YU_redirect('auth');
			} else {
				$this->auth->destroy_session();
				YU_redirect('auth');
			}
		}

		function join($invite_code = null) {
			if(!$this->is_authenticated) {
				if(!$invite_code) {
					# Lets get some prefilled information

				} else {
					#
				}

				if(!empty($_POST)) {
					# This means we're submitting the form
					$user_details = array(
						'username' => strtolower($_POST['username']),
						'password' => $_POST['password'],
						'team_id' => $this->team_data[0]['team_id'],
						'user_email' =>  $_POST['email'],
						'user_role' => "member",
						'user_firstname' => $_POST['firstname'],
						'user_lastname' => $_POST['lastname']
					);

					$json_return = array();
					$res = $this->auth->create_user($user_details);
					if($res['success']) {
						$authenticate_return = $this->auth->authenticate(array(
							'username' => $user_details['username'],
							'password' => $user_details['password'],
							'token' => $_POST['token'],
							'team' => $user_details['team_id']
						));
						if($authenticate_return['status'] == 1) {
							# We successfully authenticated, the front end will redirect to the home page
								$json_return['status'] = 'SUCCESS';
								$json_return['cookie'] = 'token='.$_POST['token'];
						} else {
							# We did not successfully authenticate, lets return some json
							$json_return['status'] = 'FAILURE';
							$json_return['reason'] = $authenticate_return['reason'];
						}
					} else {
						$json_return['status'] = 'FAILURE';
						$json_return['reason'] = $res;
					}
					echo json_encode($json_return);
				} else {
					# Lets load the interface
					$this->view->load('auth/join_screen', $this->page_vars);
				}
			} else {
				YU_redirect('home');
			}
		}

		function test() {

		}
	}
