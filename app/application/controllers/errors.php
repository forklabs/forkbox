<?php require(PROTECT);
	class Errors extends Controller {
		function __construct($code = null) {
			parent::__construct();
			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();
			$this->user_id = $this->token_data['token_data']['session_user'];
			$userdata = $this->users->get_user_data($this->user_id);
		}

		function index() {
			$this->view->load('dashboard');
		}

	}
