<?php require(PROTECT);

	class Data extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();

			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();

			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			if($this->team_data = $this->team->get_team_data($team_domain)[0])
				$team_id = $this->team_data['team_id'];
			else {
				header("Location: http://forkboxes.com/");
			}

			if($this->token_data['is_authenticated'] == 0)
				YU_redirect('auth');
			else
					$this->user_id = $this->token_data['token_data']['session_user'];

					$this->header_vars = new stdClass();
					$userdata = $this->users->get_user_data($this->user_id);
					$this->header_vars->userdata = $userdata;

		}

		function index() {
			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('database/list', $this->users->get_user_connections($this->user_id, 'db'));
			$this->view->load('fbx_footer');
		}

		function test() {

		}
	}
