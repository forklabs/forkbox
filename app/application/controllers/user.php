<?php require(PROTECT);

	# Our default controller

	# Note: The default controller does not accept parameters such as http://forklabsllc.com/parameter1/parameter2
	# This means index($param1, $param2) would be invalid.

	class User extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();
			$this->users = $this->model('user_model');
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();
			$this->userdata = $this->token_data['userdata'];
			header('Content-Type: application/json');
		}

		function index() {
			if(!$this->userdata) {
				$this->userdata = array('logged_in' => false, 'reason' => 'Not Logged In');
				$this->userdata['user_id'] = 0;
				$this->userdata['username'] = 'Guest';
			} else {
				$this->userdata['logged_in'] = true;
			}
			unset($this->userdata['user_password']);

			echo json_encode($this->userdata);
		}
	}
