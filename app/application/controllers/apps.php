<?php require(PROTECT);

	# Our Apps System
	# This is what MiniCMS will use to manage Apps.

	class Apps extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();

			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();

			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			$this->team_domain = $team_domain;

			if($this->team_data = $this->team->get_team_data($team_domain)[0])
				$team_id = $this->team_data['team_id'];
			else
				header("Location: {$_SERVER['SERVER_NAME']}");

			if($this->token_data['is_authenticated'] == 0)
				YU_redirect('auth');
			else
				$this->user_id = $this->token_data['token_data']['session_user'];

				$this->header_vars = new stdClass();
				$userdata = $this->users->get_user_data($this->user_id);
				$this->header_vars->userdata = $userdata;
		}

		function index() {
			# Lets start off by showing a list of installed apps
			$this->view->load('fbx_header', $this->header_vars);
			$this->view->load('apps/apps_list', $this->users->get_user_connections($this->user_id, 'app'));
			$this->view->load('fbx_footer');
		}

		function l($app_slug=null, $connection_id=null, $page=null, $arg1=null, $arg2=null, $arg3=null, $arg4=null, $arg5=null) {
			# Lets check to see if this user has the app registered to them
			$user_apps = $this->users->get_user_connections($this->user_id, 'app');
			$user_sub = false;

			if(!$connection_id) {
				echo "Connection ID not specified";
			} else {
				if($user_apps) {
					foreach($user_apps as $user_app) {
						if(in_array($app_slug, $user_app) && $user_app['connection']['connection_id'] == $connection_id) {
							$user_sub = true;
							$app = $user_app;
						}
					}
				}
			}

			if(!$user_sub) {
				echo "You are not subscribed to this App.";
			} else {
				// This is the url to the actual app, it will contain a JSON object defining the app.
				$app_uri = $app['app_uri'];
				# We need to get the auth key
				$auth_key = json_decode($app['connection']['connection_auth'])->auth_key;
				# Do the request and get the app_structure object that comes from the app.json
				if(ENVIRONMENT == 'development')
					$app_json = 'app.dev.json';
				else
					$app_json = 'app.json';

				$app_structure = file_get_contents($app_uri.'/'.$app_json);

				// This will serve as the home.xml
				$xml_content = '';
				$app = json_decode($app_structure)->app;
				$app_pages = $app->pages;
				if(is_null($page))
					$page = "/";
				else
					$page = "/".$page;

				# Are there any GET params passed?
				if(substr_count($page, '?')) {
						$get_explode = explode('?', $page);
						$page = $get_explode[0];
						$get_params = $get_explode[1];
						$get_params = $get_params . '&auth_key='.$auth_key.'&team='.$this->team_domain;
				} else {
					$get_params = 'auth_key='.$auth_key.'&team='.$this->team_domain;
				}

				$out = array();
				foreach($app_pages as $app_page) {
					if($app_page->path == $page) {
						$title = $app_page->title;
						$view = $app_page->view;

						# For passing dynamic page params
						$num_params = substr_count($view, '{$');
						if($num_params > 0) {
							$avail_params = array($page, $arg1, $arg2, $arg3, $arg4, $arg5);
							for($x=1;$x<=$num_params;$x++) {
								$view = str_replace('{$'.$x.'}', $avail_params[$x], $view);
							}
						}

						if($get_params)
							$view .= '?'.$get_params;

						$out['view_path'] = $view;
						$out['view_html'] = file_get_contents($out['view_path']);

						# Lets replace the links
						$num_links = substr_count($out['view_html'], '{link');
						if($num_links > 0) {
							$pattern = '/\{link:(.*)\}/';
							$replacement = '/apps/l/'.$app_slug.'/'.$connection_id.'${1}';
							$out['view_html'] = preg_replace($pattern, $replacement, $out['view_html']);
						}
						# Lets create the {thisapp:}
						$num_thisapp = substr_count($out['view_html'], '{thisapp');
						if($num_thisapp > 0) {
							$pattern = '/\{thisapp\}/';
							$replacement = '/apps/l/'.$app_slug.'/'.$connection_id;
							$out['view_html'] = preg_replace($pattern, $replacement, $out['view_html']);
						}
					}
				}


				$this->header_vars->title = $title;
				(!$json ? $this->view->load('fbx_header', $this->header_vars) : '');
				echo $out['view_html'];
				(!$json ? $this->view->load('fbx_footer') : '');
			}
		}
	}
