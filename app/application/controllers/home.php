<?php require(PROTECT);

	# Our default controller

	# Note: The default controller does not accept parameters such as http://forklabsllc.com/parameter1/parameter2
	# This means index($param1, $param2) would be invalid.

	class Home extends Controller {
		function __construct() {
			# We can load other resources here
			parent::__construct();
			$this->apps = $this->model('apps_model');
			$this->users = $this->model('user_model', array('apps' => $this->apps));
			$this->team = $this->model('team_model');

			$this->token_data = $this->auth->is_authenticated();
			$this->user_id = $this->token_data['token_data']['session_user'];
			$userdata = $this->users->get_user_data($this->user_id);
		}

		function index() {
			$this->view->load('dashboard');
		}

		# This is necessary if you have CONFIG/ERROR_OVERRIDE set to true.
		# To set error override to true, visit application/config/config.php line: 35
		function error_override() {
			echo '404 NOT FOUND';
		}
	}
