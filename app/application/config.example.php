<?php require(PROTECT);

 /*	-------------------------------------- *
  *		Environment Configuration Script		 *
  * -------------------------------------- */

	$_ENV = [];
	$_ENV['DB_HOST'] = 'forkbox-mysql';
	$_ENV['DB_USER'] = 'root';
	$_ENV['DB_PASSWORD'] = 'test';
	$_ENV['DB_NAME'] = 'forkbox';
	$_ENV['DB_ENABLE'] = 'false';


	# Path of the installation from webserver root
	# (No trailing slashes, leave blank if its just "/".)
	define('YU_PATH', '');

	# URI stuff

	define('DEFAULT_CONTROLLER', 'home');
	define('DEFAULT_METHOD', 'index');

	# Autoload Array

	$CONFIG_autoload = array(
		'database',
		'authlib'
		);

	define('DB_HOST', $_ENV['DB_HOST']);
	define('DB_USER', $_ENV['DB_USER']);
	define('DB_PASSWORD', $_ENV['DB_PASSWORD']);
	define('DB_NAME', $_ENV['DB_NAME']);
	define('DB_ENABLE', $_ENV['DB_ENABLE']);

	# Error Override
	define('YU_ERROR_OVERRIDE', true);
