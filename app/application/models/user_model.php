<?php require(PROTECT);

class User_model Extends Model {
	function __construct($models=null) {
		parent::__construct();
		$this->handler = $this->db->handler;

		# Lets include our other models
		if(!is_null($models)) {
			if(isset($models['apps']))
				$this->apps = $models['apps'];
		}
	}

	# Return apps this user is subscribed to
	function get_user_connections($user_id, $type=null, $app_slug=null) {
		if(!isset($this->apps))
			exit('Import APPS Model to use this function.');

		$sql = 'SELECT * FROM forkbox_users WHERE user_id = :user_id';
		$sth = $this->handler->prepare($sql);
		$sth->execute(array('user_id' => $user_id));
		if($result = $sth) {
			$userdata = $result->fetch(PDO::FETCH_ASSOC);
			# DEPRECATED - $user_apps = explode(',', $userdata['user_appdata']);
			# Lets construct an SQL query to get app connections
			switch($type) {
				case 'db':
					$sql = '	SELECT *
										FROM forkbox_connections
										WHERE (connection_type = "db")
										AND
										(
											connection_auth LIKE "%\"admin\"\:\"'.$user_id.'\"%"
										OR(
											connection_auth LIKE "%\"team\"\:\"'.$userdata['team_id'].'\"%"
											AND
											connection_auth LIKE "%\"role\"\:\"'.$userdata['team_role'].'\"%"
										))';

					$sth = $this->handler->prepare($sql);
					$exec_arr = array(
						'user_id' => $user_id,
						'team_id' => $userdata['team_id'],
						'user_team_role' => $userdata['team_role']
					);

					$sth->execute($exec_arr);

					if($sth) { # Everything worked out okay, we have Databases
						$user_connections = $sth->fetchAll(PDO::FETCH_ASSOC);

						return $user_connections;
					} else {
						return false;
					}
				break;
				case 'app':
				default:
					$exec_arr = array(
						'user_id' => $user_id,
						'team_id' => $userdata['team_id'],
						'user_team_role' => $userdata['team_role']
					);

					$sql = '	SELECT *
										FROM forkbox_connections
										WHERE connection_type = "app"
										AND connection_auth LIKE "%\"admin\"\:\"'.$user_id.'\"%"
										OR connection_auth LIKE "%\"team\"\:\"'.$userdata['team_id'].'\"%"
										AND connection_auth LIKE "%\"role\"\:\"'.$userdata['team_role'].'\"%"
									';
					$slug_matched = false;

					$sth = $this->handler->prepare($sql);
					$sth->execute($exec_arr);

					if($sth) { # Everything worked out okay, we have Apps
						$user_connections = $sth->fetchAll(PDO::FETCH_ASSOC);
						#print_r($user_connections);

						$apps = array();
						$user_apps = array();
						foreach($user_connections as $connection) {
							$auth_data = json_decode($connection['connection_auth']);
							$user_apps[] = array('app_id' => $auth_data->app_id, 'user_connection' => $connection);
						}
						#print_r($apps);
						foreach($user_apps as $user_app) {
							$sql = 'SELECT * FROM forkbox_apps WHERE app_id = ' . $user_app['app_id'];
							$result = $this->handler->query($sql);
							if($result) {
								$app_data = $result->fetch(PDO::FETCH_ASSOC);
								# Lets grab the app app_author
								$sql = 'SELECT * FROM forkbox_users WHERE user_id = ' . $app_data['app_author'];
								$result = $this->handler->query($sql);
								$app_data['app_author'] = 'Anonymous';

								if($result) {
									$author_data = $result->fetch(PDO::FETCH_ASSOC);
									$app_data['app_author'] = $author_data['username'];
								}
								$app_data['connection'] = $user_app['user_connection'];
								$app_data['connection']['connection_details'] = json_decode($app_data['connection']['connection_details']);
								$app_data['connection']['connection_auth'] = json_decode($app_data['connection']['connection_auth']);

								$apps[] = $app_data;
							}
						}

						if($app_slug) {
							$matched_app = null;

							foreach($apps as $app) {
								$details_json = $app['connection']['connection_details'];
								$is_match = false;
								#print_r($app);
								foreach($details_json as $key => $val) {
									switch($key) {
										case 'slug':
											if($app_slug == $val) {
												$is_match = true;
											}
										break;
									}
								}
								if($is_match) $matched_app = $app;
							}

							if(!is_null($matched_app)) {
								$apps = $matched_app;
							}
						}
						return $apps;
					} else {
						return false;
					}
				}
		} else {
			return false;
		}
	}

	function get_user_data($user_id) {
		$sql = "SELECT * FROM forkbox_users WHERE user_id = :user_id";
		$sth = $this->handler->prepare($sql);
		$sth->execute(array('user_id' => $user_id));

		if($result = $sth->fetch(PDO::FETCH_ASSOC)) {
			$sql = "SELECT * FROM forkbox_teams WHERE team_id = :team_id";
			$sth_team = $this->handler->prepare($sql);
		#	print_r($result);

			$sth_team->execute(array('team_id' => $result['team_id']));

			if($team_result = $sth_team->fetch(PDO::FETCH_ASSOC)) {
				$result['team_name'] = $team_result['team_name'];
				$result['team_logo'] = $team_result['team_logo'];
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
?>
