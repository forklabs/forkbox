<?php require(PROTECT);

class Apps_model Extends Model {
	function __construct() {
		parent::__construct();
		$this->handler = $this->db->handler;
	}

	function test() {
		echo "<br>Apps model works<br>";
		echo $this->db->test();
	}

	function get_app_data($app_id) {
		$sql = "
			SELECT f.*, u.username
			FROM forkbox_apps f
			JOIN forkbox_users u
			ON u.user_id = f.app_author
			WHERE app_id = :app_id";
		$sth = $this->handler->prepare($sql);
		$sth->execute(array('app_id' => $app_id));

		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
