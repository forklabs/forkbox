<?php require(PROTECT);

class Team_model Extends Model {
	function __construct($models=null) {
		parent::__construct();
		$this->handler = $this->db->handler;

		# Lets include our other models
		if(!is_null($models)) {
			if(isset($models['apps']))
				$this->apps = $models['apps'];
		}
	}

	# Return apps this user is subscribed to
	function get_team_data($team_domain=null) {
		if($team_domain === null) {
			$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
			if(strtolower($team_domain) == 'login')
				$team_domain = 'my';
		}

    $sql = 'SELECT * FROM forkbox_teams WHERE team_domain = :team_domain';
    $sth = $this->handler->prepare($sql);
		$sth->execute(array('team_domain' => $team_domain));

    if($team_data = $sth->fetchAll(PDO::FETCH_ASSOC)) {
      return $team_data;
    } else {
      return false;
    }
	}

	function get_team_members($team_id) {
		$sql = 'SELECT * FROM forkbox_users WHERE team_id = :team_id';
		$sth = $this->handler->prepare($sql);
		$sth->execute(array('team_id' => $team_id));
		if($team_members = $sth->fetchAll(PDO::FETCH_ASSOC)) {
			return $team_members;
		} else {
			return false;
		}
	}

	function fetch_groups($team_id) {
		$sql = 'SELECT team_groups FROM forkbox_teams WHERE team_id = :team_id';
		$sth = $this->handler->prepare($sql);
		$sth->execute(array('team_id' => $team_id));
		if($team_groups = $sth->fetch(PDO::FETCH_ASSOC)) {
			$team_groups = json_decode($team_groups['team_groups']);
			$team_owner = new stdClass();
			$team_owner->group_id = 1;
			$team_owner->group_name = 'owner';

			return $team_groups;
		} else {
			return false;
		}
	}
}
?>
