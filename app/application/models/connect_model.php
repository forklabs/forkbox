<?php require(PROTECT);

class Connect_model Extends Model {
	function __construct($models=null) {
		parent::__construct();
		$this->handler = $this->db->handler;

		# Lets include our other models
		if(!is_null($models)) {
			if(isset($models['team'])) {
				$this->team = $models['team'];

				$team_domain = strtolower(explode('.', $_SERVER['SERVER_NAME'])[0]);
				$this->team_data = $this->team->get_team_data($team_domain)[0];
			}
			if(isset($models['apps'])) {
				$this->apps = $models['apps'];
			}
		}
	}

  function create_connection($connection_data, $user_id, $access_level=null, $access_param = null) {
      # What type of connection do we have?
      switch($connection_data['connection_type']) {
        case 'db':
          $connection_details = '{"host":"'.$connection_data['db_host'].'","user":"'.$connection_data['db_user'].'","password":"'.$connection_data['db_password'].'","name":"'.$connection_data['db_name'].'"}';
          # Lets create an auth key
          $auth_key = md5('auth-'.time());
          $auth_details = '{"admin":"'.$user_id.'"';

          if(strtolower($access_level) == 'team') {
            $auth_details .= ',"team":"'.$access_param.'"';
            echo $access_param;
          }

          $auth_details .= ',"auth_key":"'.$auth_key.'"}';
          echo $auth_details;
          if(!$connection_data['connection_name'])
            return false;

          $exec_arr = array(
            'connection_name' => $connection_data['connection_name'],
            'connection_type' => 'db',
            'connection_details' => $connection_details,
            'connection_auth' => $auth_details
          );

        break;
				case 'app':
					// Let's parse the groups
					$groups = $connection_data['groups'];
					if(strlen($groups) > 0) {
						$parsed_groups = array();
						if(strpos($groups, ',')) {
							$parsed_groups = explode(',', $groups);
							$x = 0;
							foreach($parsed_groups as $pg) {
								$parsed_groups[$x] = trim($pg, ' ');
								$x++;
							}
						} else {
							$parsed_groups[] = $groups;
						}
					} else {
						$parsed_groups = null;
					}

					// Let's parse the description
					if(strlen($connection_data['description']) > 0) {
						$connection_description = $connection_data['description'];
					} else {
						$connection_description = $this->apps->get_app_data($connection_data['app_id'])[0]['app_name'];
					}

					// Let's parse the description
					if(strlen($connection_data['name']) > 0) {
						$connection_name = $connection_data['name'];
					} else {
						$connection_name = $this->apps->get_app_data($connection_data['app_id'])[0]['app_name'];
					}

					$connection_auth = array(
						'admin' => $user_id,
						'app_id' => $connection_data['app_id'],
						'auth_key' => md5(rand() + time())
					);

					if($parsed_groups) {
						$team_groups = $this->team_data['team_groups'];
						$team_groups = json_decode($team_groups);
						print_r($parsed_groups);
						$loaded_groups = array();
						foreach($parsed_groups as $grp) {
							foreach($team_groups as $tgrp) {
								if($grp == $tgrp->group_name)
									$loaded_groups[] = $tgrp->group_id;
							}
						}

						$connection_auth['auth_groups'] = $loaded_groups;
					}

					$exec_arr = array(
						'connection_name' => $connection_name,
						'connection_type' => 'app',
						'connection_details' => $connection_description,
						'connection_auth' => json_encode($connection_auth)
					);

				break;
        case null:
        default:
        return false;
      }

      $sql = 'INSERT INTO forkbox_connections (
        connection_name,
        connection_type,
        connection_details,
        connection_auth)
      VALUES (
        :connection_name,
        :connection_type,
        :connection_details,
        :connection_auth
      )';
      $sth = $this->handler->prepare($sql);
      $result = $sth->execute($exec_arr);

      if($result) {
				$sql = 'SELECT connection_id FROM forkbox_connections WHERE connection_auth LIKE :auth_key';
				$sth = $this->handler->prepare($sql);
				$sth->bindValue(':auth_key', '%'.$connection_auth['auth_key'].'%');

				$result = $sth->execute();

				if($result) {
					return array(
						"data" => $sth->fetchAll(PDO::FETCH_ASSOC)
					);
				} else {
					return array("error" => $this->handler->errorInfo());
				}
      } else {
        return array("error" => $this->handler->errorInfo());
      }
  }

	function get_connections($type, $search=null) {
		$searchTerms = array();
		switch($type) {
			case 'app':
				$sql = "	SELECT
										app_id,
										app_name,
										app_description,
										app_author,
										app_slug,
										app_creation_time,
										app_uri
				 					FROM forkbox_apps";
				if($search) {
					$sql .= ' WHERE
											app_name LIKE :searchTerms
										OR
											app_description LIKE :searchTerms';
					$searchTerms['searchTerms'] = $search;
				}
			break;
		}

		$sth = $this->handler->prepare($sql);

		if($search) {
			$sth->bindValue(':searchTerms', '%'.$searchTerms['searchTerms'].'%');
			$sth->execute();
		} else {
			$sth->execute($searchTerms);
		}
		if($sth) {
			return array(
				"data" => $sth->fetchAll(PDO::FETCH_ASSOC)
			);
		} else {
			return array("error" => $this->handler->errorInfo());
		}
	}

	function test() {
		echo "<br>Connect model works<br>";
		echo $this->db->test();
	}
}
