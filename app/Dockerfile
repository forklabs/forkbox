FROM ubuntu:latest

LABEL name=forkbox-apache \
      version=1.0.0 \
      maintainer="Devontrae Walls <devontrae@forklabscreative.com>"

ARG ENVIRONMENT=production
ARG DB_HOST=forkbox-mysql
ARG DB_NAME=forkbox
ARG DB_PORT=3306
ARG DB_USER=root
ARG DB_PASS=test

ENV ENVIRONMENT=$ENVIRONMENT
ENV DB_HOST=$DB_HOST
ENV DB_NAME=$DB_NAME
ENV DB_PORT=$DB_PORT
ENV DB_USER=$DB_USER
ENV DB_PASS=$DB_PASS

ENV PHINX_DB_HOST=$DB_HOST
ENV PHINX_DB_NAME=$DB_NAME
ENV PHINX_DB_PORT=$DB_PORT
ENV PHINX_DB_USER=$DB_USER
ENV PHINX_DB_PASS=$DB_PASS

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential gperf \
  software-properties-common apt-utils language-pack-en-base \
  && apt-get clean \
  && rm -Rf /var/lib/apt/lists/*

# Install PHP PPA
RUN LC_ALL=en_US.UTF add-apt-repository ppa:ondrej/php

# Install Apache,
RUN apt-get update \
  && export RUNLEVEL=1 \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y vim curl memcached \
    zip imagemagick php7.1 php7.1-soap php7.1-curl php7.1-mcrypt \
    php7.1-gd php7.1-imagick php7.1-json php7.1-xml php7.1-mbstring php-memcached \
    php7.1-mysqli php7.1-mysqlnd php7.1-pdo php7.1-pdo-mysql php-xdebug \
  apache2 libapache2-mod-php7.1\
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/*

# Configure Apache
RUN a2enmod setenvif rewrite php7.1 headers ssl authz_groupfile proxy

EXPOSE 80/TCP 443/TCP

ADD docker/execute.sh /root/execute.sh
ADD docker/expose-env.conf /etc/apache2/conf-enabled/expose-env.conf

RUN export DB_HOST=${DB_HOST}
RUN export DB_NAME=${DB_NAME}
RUN export DB_USER=${DB_USER}
RUN export DB_PASS=${DB_PASS}
RUN export DB_PORT=${DB_PORT}
RUN export ENVIRONMENT=${ENVIRONMENT}

RUN export PHINX_DB_HOST=${DB_HOST}
RUN export PHINX_DB_NAME=${DB_NAME}
RUN export PHINX_DB_USER=${DB_USER}
RUN export PHINX_DB_PASS=${DB_PASS}
RUN export PHINX_DB_PORT=${DB_PORT}

RUN echo $ENVIRONMENT
RUN cat /etc/environment

RUN rm /etc/apache2/sites-enabled/000-default.conf
ADD docker/forkbox-vhost.conf /etc/apache2/sites-available/forkbox.conf

RUN ln -s /etc/apache2/sites-available/forkbox.conf /etc/apache2/sites-enabled/forkbox.conf
RUN rm /etc/apache2/apache2.conf /var/www/html/index.html

ADD docker/forkbox-apache2.conf /etc/apache2/apache2.conf

WORKDIR /var/www/html

COPY . .

CMD ["/bin/bash", "/root/execute.sh"]
