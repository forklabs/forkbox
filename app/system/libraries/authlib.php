<?php require(PROTECT);

class Authlib extends Libraries {
	function __construct() {
		parent::__construct();
		$this->db = new Database();
		$this->handler = $this->db->handler;

		# Lets retrieve the session token
		$token = null;
		$headers = apache_request_headers();

		if(isset($headers['Authorization'])){
			$matches = array();
			preg_match('/Bearer (.*)/', $headers['Authorization'], $matches);
			if(isset($matches[1])){
				$this->token = $matches[1];
			}
		} else {
			$this->token = $_COOKIE['token'];
			if(!$this->token)
				$this->token = 0;
		}

	}

	function is_authenticated() {
		#echo $this->token;
		# The return array
		$return = array();
		# Get the cookie
		if(!$this->token) {
			$return['is_authenticated'] = false;
			$return['error_reason'] = 'Bearer Token Not Present';
		} else {
			# Check the token against database
			$sql = 'SELECT * FROM forkbox_sessions WHERE session_token LIKE "'.$this->token.'"';

			$sth = $this->handler->query($sql);
			$result = $sth->fetch(PDO::FETCH_ASSOC);

			if($result) {
				$token_expired = 0;

				$token_expiry = strtotime($result['session_time_initiated']) + (60*60*24*14);
				if($token_expiry < time() || $result['session_user'] == 0) {
					$token_expired = 1;
					$sql = 'UPDATE forkbox_sessions SET session_user = 0 WHERE session_token LIKE "'.$this->token.'"';
					$sth = $this->handler->query($sql);
					$return['is_authenticated'] = 0;
					$return['error_reason'] = 'Token Expired';
				} else {
					$return['is_authenticated'] = 1;

					# We are authenticated, so lets return some user data
					$user_id = $result['session_user'];
					$sql = 'SELECT * FROM forkbox_users WHERE user_id = :user_id';
					$sth = $this->handler->prepare($sql);
					$sth->execute(array('user_id' => $user_id));
					$userdata = $sth->fetch(PDO::FETCH_ASSOC);

					if($userdata) {
						$return['userdata'] = array(
							'user_id' => $userdata['user_id'],
							'username' => $userdata['username'],
							'user_firstname' => $userdata['user_firstname'],
							'user_lastname' => $userdata['user_lastname']
						);
					} else {
						$return['is_authenticated'] = 0;
						$return['error_reason'] = 'Not a real user.';
					}
				}
			} else {
				$return['is_authenticated'] = 0;
				$return['error_reason'] = 'Token not recognized';
			}
		}

		return $return;
	}

	function create_user($user_details) {
		$exec_arr = array(
			'username' => $user_details['username'],
			'password' => md5($user_details['password']),
			'team_id' => $user_details['team_id'],
			'user_email' => $user_details['user_email'],
			'user_role' => $user_details['user_role'],
			'user_firstname' => $user_details['user_firstname'],
			'user_lastname' => $user_details['user_lastname']
		);
		# Make sure this username does not exist in the team
		# Make sure the Email Address does not exist on the team
		$sql = '	SELECT *
							FROM forkbox_users
							WHERE
									username LIKE :username
									AND team_id = :team_id
							OR	user_email LIKE :user_email
									AND team_id = :team_id';

		$sth = $this->handler->prepare($sql);
		$sth->execute(array(
			'username' => $exec_arr['username'],
			'team_id' => $exec_arr['team_id'],
			'user_email' => $exec_arr['user_email']
		));

		$result = $sth->fetchAll(PDO::FETCH_ASSOC);

		if($result) {
			$field = false;
			$fields = false;

			if($result[0]['username'] == $exec_arr['username']) {
				$what = 'Username';
				$field = 'username';
			}

			if($result[0]['user_email'] == $exec_arr['user_email']) {
				if($result[0]['username'] == $exec_arr['username']) {
					$what .= ' and Email Address';
					$fields = array('username', 'email');
					$field = false;
				} else {
					$what .= 'Email Address';
					$field = 'email';
				}
			}

			return array(
				'success' => false,
				'message' => 'That ' . $what . ' is already registered.',
				'field' => $field,
				'fields' => $fields
			);
		} else {
			# create user
			# Lets insert the user now
			$sql = 'INSERT INTO forkbox_users (
								username,
								user_password,
								team_id,
								user_email,
								team_role,
								user_firstname,
								user_lastname
							) VALUES (
								:username,
								:password,
								:team_id,
								:user_email,
								:user_role,
								:user_firstname,
								:user_lastname
							)';
			$sth = $this->handler->prepare($sql);
			$sth->execute($exec_arr);

			if($sth) {
				# It worked, lets fetch the new user
				$sql = '	SELECT *
									FROM forkbox_users
									WHERE username
									LIKE :username
										AND user_password = :password
										AND user_email = :user_email';
				$sth = $this->handler->prepare($sql);
				$sth->execute(array(
					'username' => $exec_arr['username'],
					'password' => $exec_arr['password'],
					'user_email' => $exec_arr['user_email']
				));

				if($sth) {
					return array('success' => true, 'user_data' => $sth->fetch(PDO::FETCH_ASSOC));
				} else {
					return array('success' => false, 'message' => 'User created, unable to retrieve the new user though. This has been forwarded to our team, and we will fix it up from here then email you when we are finished!', 'error' => $sth->error);
				}
			} else {
				return array('success' => false, 'message' => 'Uh-Oh! We couldn\'t create your account. This is probably our fault, contact us at contact@forkboxes.com if you see this message!', 'error' => $sth->error);
			}
		}
	}

	function create_team($team_details) {
		# When creating a team, we create the first user as well
		$team_owner['team_id'] = '0';
		$team_owner = $this->create_user($team_details['team_owner']);

		if($team_owner) {
			# The team owner was created
			$exec_arr = array(
				'team_name' => $team_details['team_name'],
				'team_domain' => $team_details['team_domain'],
				'team_admin' => $team_owner['user_id']
			);
			# We add the logo later

			# Make sure the Team Name is unique
			# Make sure the team_domain is unique
			# Make sure the team_admin is unique
			$sql = '	SELECT COUNT(*)
								FROM forkbox_teams
								WHERE
									team_name LIKE :team_name
								OR	team_domain LIKE :team_domain
								OR team_admin = :team_admin';

			$sth = $this->handler->prepare($sql);
			$sth->execute($exec_arr);
			$result = $this->fetch(PDO::FETCH_ASSOC);

			# Was the team created successfully
			if($result) {
				echo "failed maybe | ";
				print_r($result);
				$success = false;
			} else {
				echo "success maybe | ";
				echo $sth->error;
				$success = true;
			}

			# Below we will create the team, and authenticate the user
			if($success) {
				$sql = 'INSERT INTO forkbox_teams (
										team_name, team_admin, team_domain, team_logo
									) VALUES (
										:team_name,
										:team_admin,
										:team_domain,
										:team_logo
									)';

				# Add the new user_id to the team_admin
				$exec_arr['team_owner'] = $team_owner['user_id'];

				$login_details = array(
					'username' => $team_owner['username'],
					'password' => $team_owner['password'],
					'token' => $this->token,
					'team' => $team_id
				);
				$this->authenticate($login_details);
			}


		} else {
			# Team owner not created, we'll return an error object
			$error = array('success' => false, 'message' => $team_owner['error_message']);
			return $error;
		}
	}

	function authenticate($login_details) {
		$return_array = array();

		if(is_array($login_details)) {
			$token = $login_details['token'];

			# Lets see if our username and passwords match a user
			$exec_arr = array(
				'email' => $login_details['email'],
				'password' => md5($login_details['password'])
			);
			$sql = 'SELECT *
							FROM forkbox_users
							WHERE (
									user_email LIKE :email
										AND user_password = :password
									)
									OR (
										username LIKE :email
											AND user_password = :password
									)';

			$sth = $this->handler->prepare($sql);
			$sth->execute($exec_arr);
			$userdata = $sth->fetch(PDO::FETCH_ASSOC);

			if($userdata) {
				# Lets create the session on the database table
				$exec_arr = array(
							'token' => $token,
							'user_id' => $userdata['user_id'],
							'session_ip' => $_SERVER['REMOTE_ADDR'],
							'session_useragent' => $_SERVER['HTTP_USER_AGENT']
				);
				$sql = '	INSERT INTO forkbox_sessions (
										session_token,
										session_user,
										session_ip,
										session_useragent
									) VALUES (
										:token,
										:user_id,
										:session_ip,
										:session_useragent
									)';

					$sth = $this->handler->prepare($sql);
					$sth->execute($exec_arr);

				$return_array['status'] = 1;
				$return_array['userdata'] = array(
					'user_id' => $userdata['user_id'],
					'username' => $userdata['username'],
					'user_firstname' => $userdata['user_firstname'],
					'user_lastname' => $userdata['user_lastname']
				);

				$return_array['result'] = $return_array['userdata'];
				$return_array['test'] = $result['user_id'];
			} else {
				# Reasons; 1 = wrong info
				$return_array['status'] = 0;
				$return_array['reason'] = 'invalid credentials';
			}
		} else {
			$return_array['status'] = 0;
			$return_array['reason'] = 2; # Reasons; 2 = internal error (login_details not array)
		}

		return $return_array;
	}

	function test() {
		echo "<br>Auth Library Works!";
	}

	function destroy_session() {
		$token_id = $this->token;
		$expiry = time() - 100;
		$sql = 'UPDATE forkbox_sessions SET session_time_expiration = 1 WHERE session_token = "'.$token_id.'"';
		$sth = $this->handler->query($sql);
		#echo "token=;expires=Thu, 01 Jan 1970 00:00:00 UTC";
	}

}
