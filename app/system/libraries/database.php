<?php require(PROTECT);

class Database extends Libraries {
	function __construct() {
		parent::__construct();

		# This is our handler; connects us to the database using PDO
		$this->handler = false;
		$this->connect($_SERVER['DB_HOST'], $_SERVER['DB_NAME'], $_SERVER['DB_USER'], $_SERVER['DB_PASS']);
		$this->handler = $this->getHandler();
	}

	function connect($host, $dbname, $username, $password) {
		$conn_str = 'mysql:host='.$host.';dbname='.$dbname;
		$handler = false;
		try{
			$handler = new PDO($conn_str, $username, $password);
			$handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			die("Database connection issue.<br /><b>");
		}

		#echo '<br><b>[DATABASE TEST]</b>: Database Connection Established...</br>';
		$this->handler = $handler;
		return true;
	}

	public function test() {
		echo 'Database Test Works';
		#echo $this->handler;
	}

	function getHandler() {
		return $this->handler;
	}
}
