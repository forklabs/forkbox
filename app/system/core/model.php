<?php require(PROTECT);

	class Model {

		public $db;

		public function __construct() {
			# This is the parent Model method
			$this->db = new Database();
		}

		public static function load($model_name, $import=null) {
			# Lets require our model
			require(MODELS . strtolower($model_name) . '.php');

			$model_obj_name = ucfirst($model_name);
			$model_obj = new $model_obj_name($import);

			return $model_obj;
		}
	}
