<?php require(PROTECT);

	class Controller {
		public function __construct() {
			global $_GET_PARAMS;
			$this->GET = $_GET_PARAMS;
			$this->auth = new Authlib();
			$this->view = new view();
		}

		public function model($model_name, $import=null) {
			return Model::load($model_name, $import);
		}
	}
