<?php	require(PROTECT);
	/*	-------------------------- 	*
	 *			ForkBox									*
	 *	By ForkLabs Creative, LLC. 	*
	 *	--------------------------	*/

	/*	--------------------------	*
	 *		  Load Errors       	*
	 *	--------------------------	*/
	 #	require(CORE_PATH . 'errors.php');

	 /*	--------------------------- *
	 *	Lets require our constants		*
	 *	---------------------------	*/
	 require(CORE_PATH . 'constants.php');

	/*	---------------------------- *
	 *		Lets require our core	 *
	 *			  functions.		 *
	 *	---------------------------- */
	 require(CORE_PATH . 'common.php');


	/*	---------------------------- *
	 *	LOAD THE LIBRARY CLASS 		 *
	 *	---------------------------- */
	 require(CORE_PATH . 'libraries.php');
	 $libraries = new Libraries();

	/*	---------------------------- *
	 *	AUTOLOADED LIBRARIES 		 *
	 *	---------------------------- */
	 require LIBRARIES . 'autoload.php';
	 //$autoload = new Autoload();

	/*	----------------------------- *
	 *	LOAD THE MODEL CLASS	 	  *
	 *	----------------------------- */
	 require(CORE_PATH . 'model.php');

	 /*	--- Initialize Model --- */
	 	//$_MODEL = new Model();
	 /* ----------------------------- */


	/*	----------------------------- *
	 *	LOAD THE VIEW CLASS		  	  *
	 *	----------------------------- */
	 require(CORE_PATH . 'view.php');


	/*	---------------------------- *
	 *	LOAD TEMPLATE CLASS			 *
	 *	---------------------------- */
	 require(CORE_PATH . 'template.php');


	/*	---------------------------- *
	 *	   Lets process our URI		 *
	 *	---------------------------- */
	require(LIBRARIES . 'uri.php');
	$uri = new uri();
	$controller = $uri->getURI('page');
	$method = $uri->getURI('action');
	$var = $uri->getURI('var');
	$args = $uri->getURI('args');
	$_GET_PARAMS = $uri->getURI('get');

	define('ACTIVE_PAGE', strtolower($controller));

	 /*	----------------------------- *
	 *	LOAD THE CONTROLLER CLASS NOW *
	 *	----------------------------- */
	 require(CORE_PATH . 'controller.php');

	 if(file_exists(CONTROLLERS . $controller . '.php')){
		 require(CONTROLLERS . $controller . '.php');
	 } else {
		 	require(CONTROLLERS . 'errors.php');
		 	$action = '';
		 	$var = '';
		 	$args = array();
		 	define('IS_ERRORED', true);
		 	$controller = 'errors';
	 }

	 $rebuild_args = array();
	 $x = 0;
	 foreach($args as $arg) {
	  if($arg !== 'api' && $arg !== '' && $arg !== $controller &&  $arg !== $method) {
	    $rebuild_args[$x] = $arg;
	    $x++;
	  }
	 }
	 empty($args);
	 $args = $rebuild_args;


	# Assuming the script made it this far, create an instance of the requested controller
	if(defined('IS_ERRORED'))
		$controller_args = '404';
	else
		$controller_args = null;

	$YU_CONTROLLER = new $controller($controller_args);

	# Now lets check to see if the method exists in this Class
	if(method_exists($YU_CONTROLLER, $method)) {
		call_user_func_array(array($YU_CONTROLLER, $method), $args);
	} else {
		if(YU_ERROR_OVERRIDE) {
			# Lets require the default controller
			$YU_DEFAULT_CONTROLLER_FILE = CONTROLLERS . DEFAULT_CONTROLLER . '.php';

			if(file_exists($YU_DEFAULT_CONTROLLER_FILE))
				require($YU_DEFAULT_CONTROLLER_FILE);
			else
				die(FATAL_ERROR_OPEN.'Cannot find <b>default</b> controller: <b>'.DEFAULT_CONTROLLER.'</b>'.FATAL_ERROR_CLOSE);

			$controller = DEFAULT_CONTROLLER;
			$YU_CONTROLLER = new $controller();

			call_user_func_array(array($YU_CONTROLLER, 'error_override'), $args);
		} else {
			echo FATAL_ERROR_OPEN."Method <b>'" . $method . "'</b> does not exist in class: <b>'" . $controller . "'</b>".FATAL_ERROR_CLOSE;
		}
	}
